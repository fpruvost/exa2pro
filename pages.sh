#!/bin/bash
mkdir -p public
emacs README.org --batch -f org-html-export-to-html --kill
mv README.html public/index.html
cp *.png public/
