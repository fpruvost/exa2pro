\documentclass[slideopt,A4,showboxes,svgnames]{beamer}


%% list of packages here
\usepackage{multirow}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{tikz}
\usetikzlibrary{svg.path}
\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue}
}

\renewcommand{\sfdefault}{lmss}
\sffamily

\setbeamersize{text margin left=1cm,text margin right=1cm}
\setbeamerfont{alerted text}{series=\bfseries}
\setbeamerfont{example text}{series=\bfseries}

\usepackage[absolute,showboxes,overlay]{textpos}

\TPshowboxesfalse
\textblockorigin{0mm}{0mm}

\hypersetup{
  allcolors=rouge_inria,
}

\newcommand{\GRAND}{\fontsize{100}{100}\selectfont}
\newcommand{\Grand}{\fontsize{80}{80}\selectfont}
\newcommand{\grand}{\fontsize{60}{60}\selectfont}

\newcommand{\start}{\setcounter{beamerpauses}{1}}
\newcommand{\local}{_i}
\newcommand{\A}{{\cal A}}
\newcommand{\Ai}{\A\local}

%% Un point rouge simple sans ombré pour les puces : OK
\title[Solving large linear systems]{Solving large linear systems}
\subtitle{Parallel solvers based on runtime systems}
\date[24/02/2021]{24/02/2021}
\author[Florent Pruvost]{}

\usetheme{inria}
%\usetheme{inria2}
%\usetheme{inria3}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Titre de la présentation avec format Inria
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}
  \titlepage
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%Plan de la présentation

%Chapitre 1
\frame{\tocpage}

\section{Context}
\frame{\sectionpage}


\begin{frame}{Research area}

  \begin{center}\textbf{Inria Team \textcolor{rouge_inria}{HiePACS}}\end{center}
  \small{\textit{High-End Parallel Algorithms for Challenging Numerical Simulations}}
  % Main idea
  \begin{center}
  \begin{columns}
    \begin{column}[c]{.25\linewidth}
      \vspace{-0.5cm}
      \begin{exampleblock}{\centering {\scriptsize Linear algebra}}
        \centering $\bf \textcolor{blue}{A} \textcolor{red}{X} = \textcolor{blue}{B}$\\
      \end{exampleblock}
      \begin{exampleblock}{\centering {\scriptsize Sequential-Task-Flow}}
      {\tiny
      \begin{semiverbatim}
      for (j = 0; j < N; j++)
        \alert{Task} (A[j]);
      \end{semiverbatim}}
      \end{exampleblock}
      \begin{exampleblock}{\centering {\scriptsize Direct Acyclic Graph}}
        \centering \includegraphics[width=0.4\linewidth]{Figs/dag1}
      \end{exampleblock}
    \end{column}
    \begin{column}[c]{.005\linewidth}
      \centering $\rightarrow$
    \end{column}
    \begin{column}[c]{.23\linewidth}
      \begin{exampleblock}{\centering Runtime systems}
        \includegraphics[width=\linewidth]{Figs/102_dag_mapping}
      \end{exampleblock}
    \end{column}
    \begin{column}[c]{.005\linewidth}
      \centering $\rightarrow$
    \end{column}
    \begin{column}[c]{.23\linewidth}
      \begin{exampleblock}{\centering Heterogeneous platforms}
      \begin{tabular}{ccc}
        \includegraphics[width=0.3\linewidth]{Figs/laptop.jpg} & & \includegraphics[width=0.3\linewidth]{Figs/NVIDIA_Tesla_K20_01.jpg} \\
        \includegraphics[width=0.3\linewidth]{Figs/cray-blade} & & \includegraphics[width=0.35\linewidth]{Figs/Tera-100-profile-3ok.jpg}
      \end{tabular}
      \end{exampleblock}
    \end{column}
  \end{columns}
  \end{center}

  \begin{center}
  \textbf{Main goal :} performances, scaling
  \end{center}

\end{frame}

\begin{frame}{Parallel linear solvers}

  \begin{exampleblock}{\centering Chameleon: \textcolor{rouge_inria}{dense matrix} solver}
    \begin{itemize}
    \item BLAS: basic scalar, vector, matrix operations
    $$ \alpha \begin{pmatrix} . \\ . \end{pmatrix}, \quad
       \begin{pmatrix} . \\ . \end{pmatrix} + \begin{pmatrix} . \\ . \end{pmatrix}, \quad
       \begin{pmatrix} . & . \\ . & . \end{pmatrix} \begin{pmatrix} . \\ . \end{pmatrix}, \quad
       \begin{pmatrix} . & . \\ . & . \end{pmatrix} \begin{pmatrix} . & . \\ . & . \end{pmatrix} $$
    \item LAPACK: linear systems $\bf AX=B$,
      square-roots, eigen val.
    \end{itemize}
  \end{exampleblock}

  \begin{exampleblock}{\centering PaStiX: \textcolor{rouge_inria}{sparse matrix direct} solver}
    \begin{itemize}
    \item linear systems $\bf AX=B$, factorization $\bf LL^T, LDL^T, LU$
    \end{itemize}
  \end{exampleblock}

  \begin{exampleblock}{\centering MaPHyS: \textcolor{rouge_inria}{sparse matrix hybrid} solver}
    \begin{itemize}
    \item linear systems $\bf AX=B$, \textbf{CG/GMRES + Preconditioner}
    \item Preconditioner = direct solver $\rightarrow$ MUMPS or PaStiX
    \end{itemize}
  \end{exampleblock}

  %\vspace{0.5cm}
  %\centering\textbf{paradigms: \textcolor{rouge_inria}{Threads}, \textcolor{rouge_inria}{CUDA}, \textcolor{rouge_inria}{MPI}}

\end{frame}

\begin{frame}{Runtimes supported by Chameleon \& PaStiX}

  \begin{footnotesize}
    \begin{exampleblock}{\centering\texttt{StarPU}}
      \begin{itemize}
      \item Inria Storm Team %% -- Inria Bordeaux - Sud-Ouest
      \item {\bf Dynamic Task Discovery}
      \item C/C++, Fortran
      \item MPI + Thread + Cuda
      \item Multiple scheduling strategies: Minimum Completion Time, Local Work
        Stealing, user defined...
      \item Computes cost models on the fly
      \end{itemize}
    \end{exampleblock}

    \begin{exampleblock}{\centering\texttt{PaRSEC}}
      \begin{itemize}
      \item {\sc ICL} -- University of Tennessee, Knoxville
      \item {\bf Parameterized Task Graph}
      \item C/C++, Fortran
      \item MPI + Thread + Cuda
      \item Scheduling strategy based on static performance model
      \end{itemize}
    \end{exampleblock}
  \end{footnotesize}

\end{frame}

\begin{frame}{Advantages of using a task-based runtime}

  \begin{itemize}
  \item Several computing kernels can be associated with the task
    \begin{itemize}
    \item \texttt{C}, \texttt{OpenCL}, \texttt{NVIDIA} \texttt{CUDA}
    \end{itemize}
  \item Execute the task graph on the available resources
  \item Address the whole computing units and the whole potential parallelisms
  \item Insulate the algorithm from the architecture and data distribution
  \item Automatic handling of data transfers
  \item Finer parallelism handling
  \end{itemize}

\end{frame}

\begin{frame}{Software development quality}

  \begin{center}
    \only<1>{\includegraphics[width=1.\linewidth]{Figs/gitlab-solverstack.png}\\
    Opensource - Hosted at \textcolor{rouge_inria}{\url{https://gitlab.inria.fr/solverstack}}}
    \only<2>{\includegraphics[width=1.\linewidth]{Figs/gitlab-chameleon-cijobs.png}\\
    Continuous integration: build, tests, coverage, etc}
    \only<3>{\includegraphics[width=1.\linewidth]{Figs/sonarqube-pastix.png}\\
    Source code checkers: SonarQube, clang-sa/tidy, cppcheck}
    \only<4>{\includegraphics[width=1.\linewidth]{Figs/gitlab-pages-chameleon.png}\\
    Performances tests once a week on \textcolor{rouge_inria}{\url{www.plafrim.fr}}}
  \end{center}

\end{frame}

\begin{frame}[fragile]{Softwares distribution}


  \begin{exampleblock}{CMake (ex: Ubuntu 20.04)}
    {\scriptsize
\begin{verbatim}
  sudo apt install git python-numpy cmake build-essential gfortran \
                   pkg-config libmkl-dev libscotch-dev libstarpu-dev
  git clone --recursive https://gitlab.inria.fr/solverstack/pastix.git
  cd pastix && mkdir build && cd build
  cmake .. -DPASTIX_WITH_STARPU=[ON|OFF] -DPASTIX_INT64=OFF && make
\end{verbatim}
    }
  \end{exampleblock}

  \begin{exampleblock}{Spack (Linux, MacOS)}
    {\scriptsize
\begin{verbatim}
  spack install chameleon pastix maphys
\end{verbatim}
  \textcolor{rouge_inria}{\url{https://gitlab.inria.fr/solverstack/spack-repo}}}
  \end{exampleblock}

  \begin{exampleblock}{GNU Guix (Linux)}
    {\scriptsize
\begin{verbatim}
  guix install chameleon pastix maphys
\end{verbatim}
  \textcolor{rouge_inria}{\url{https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free}}}
  \end{exampleblock}


\end{frame}

\section{Chameleon}
\frame{\sectionpage}

\begin{frame}{Software ecosystem}

  \textcolor{rouge_inria}{\url{https://gitlab.inria.fr/solverstack/chameleon}}

  \begin{center}
    \includegraphics[width=1.\linewidth]{Figs/chameleon_dep}
  \end{center}

  \begin{itemize}
  \item choose between 4 runtime systems
  \item MPI and CUDA/cuBLAS optionals
  \end{itemize}

\end{frame}

\begin{frame}{Software features}

  \begin{itemize}
  \item \textbf{Language}: C/CMake, Fortran interface
  \item \textbf{Algorithms}: GEMM, POTRF, GETRF, GEQRF, GESVD, ...
  \item \textbf{Matrices forms}: general, symmetric, triangular
  \item \textbf{Precisions}: simple, double, complex, double complex
  \end{itemize}

  See list of available routines: \textcolor{rouge_inria}{\url{https://solverstack.gitlabpages.inria.fr/chameleon/users_guide.html}}

\end{frame}

\begin{frame}[fragile]{Under the hood, Task-based Cholesky example}
  \begin{columns}
    \column[t]{7cm}
    \begin{minipage}{7cm}
      \begin{itemize}
      \item work on tiles $\rightarrow$ kernels (CPU, GPU)
      \item task graph $\rightarrow$ runtime system
      \end{itemize}
          {\small
            \begin{semiverbatim}
       for (j = 0; j < N; j++) \{
       \alert<2,12>{  POTRF (RW,A[j][j]);}
       for (i = j+1; i < N; i++)
       \alert<3,4,5>{    TRSM (RW,A[i][j], R,A[j][j]);}
       for (i = j+1; i < N; i++) \{
       \alert<6,9,11>{    SYRK (RW,A[i][i], R,A[i][j]);}
       for (k = j+1; k < i; k++)
       \alert<7,8,10>{      GEMM (RW,A[i][k],
         R,A[i][j], R,A[k][j]);}
       \}
       \}
       \alert<13>{__wait__();}
          \end{semiverbatim}}
    \end{minipage}
    \column[t]{4cm}
    \begin{minipage}{4cm}
      \only<1>{\includegraphics[width=4cm]{Figs/matrix-step-0}}%
      \only<2>{\includegraphics[width=4cm]{Figs/matrix-step-1}}%
      \only<3>{\includegraphics[width=4cm]{Figs/matrix-step-2}}%
      \only<4>{\includegraphics[width=4cm]{Figs/matrix-step-3}}%
      \only<5>{\includegraphics[width=4cm]{Figs/matrix-step-4}}%
      \only<6>{\includegraphics[width=4cm]{Figs/matrix-step-5}}%
      \only<7>{\includegraphics[width=4cm]{Figs/matrix-step-6}}%
      \only<8>{\includegraphics[width=4cm]{Figs/matrix-step-7}}%
      \only<9>{\includegraphics[width=4cm]{Figs/matrix-step-8}}%
      \only<10>{\includegraphics[width=4cm]{Figs/matrix-step-9}}%
      \only<11>{\includegraphics[width=4cm]{Figs/matrix-step-10}}%
      \only<12>{\includegraphics[width=4cm]{Figs/matrix-step-11}}%
      \only<13>{\includegraphics[width=4cm]{Figs/matrix-step-12}}%
    \end{minipage}
  \end{columns}
\end{frame}

\begin{frame}{Software performances examples}

  \begin{center}
    \only<1>{\includegraphics[width=1.0\linewidth]{Figs/sgemm_openmpi_occigen_1}\\
      SGEMM 1-4 Occigen nodes}
    \only<2>{\includegraphics[width=1.\linewidth]{Figs/sgemm_openmpi_occigen_2}\\
      SGEMM 8-24 Occigen nodes}
    \only<3>{\includegraphics[width=1.\linewidth]{Figs/sgemm_openmpi_occigen_3}\\
      SGEMM 32-128 Occigen nodes}
    \only<4>{\includegraphics[width=0.9\linewidth]{Figs/plot_tera_perfs_both}\\
      DPOTRF 144 TERA-100 nodes with 2 Tesla M2090 GPUs}
    \only<5>{\includegraphics[width=0.9\linewidth]{Figs/potri_async.png}\\
      Algorithms can be pipelined}

  \end{center}

\end{frame}

\section{PaStiX}
\frame{\sectionpage}

\begin{frame}{Software ecosystem}

  \vspace{0.5cm}
  \textcolor{rouge_inria}{\url{https://gitlab.inria.fr/solverstack/pastix}}

  \begin{center}
    \includegraphics[width=1.\linewidth]{Figs/pastix_dep}
  \end{center}

  \vspace{-1.cm}
  \begin{itemize}
  \item choose between 2 runtime systems
  \item choose between 2 graph partitioners
  \item MPI and CUDA/cuBLAS optionals
  \end{itemize}

\end{frame}

\begin{frame}{Software features}

  \begin{itemize}
  \item \textbf{Language}: C/CMake, Fortran/Python/Julia interfaces
  \item \textbf{Algorithms}: POTRF, GETRF, TRSM, ...
  \item \textbf{Matrices forms}: general, symmetric, triangular
  \item \textbf{Storage formats}: CSC, CSR, and IJV
  \item \textbf{Precisions}: simple, double, complex, double complex
  \item \textbf{Low-Rank compression}
  \end{itemize}

  See list of available routines: \textcolor{rouge_inria}{\url{https://solverstack.gitlabpages.inria.fr/pastix/}}

\end{frame}

\begin{frame}{Under the hood}

  \vspace{0.5cm}
  \small{
  \begin{block}{General approach}
    \begin{enumerate}
    \item Use the nested dissection process to partition a sparse matrix
    \item Use the minimum degree solution when leaves are small enough
    \item Generate a symbolic block structure of $L$
    \end{enumerate}
  \end{block}
  \begin{figure}[H]
    \centering \includegraphics[scale=0.4]{Figs/SymbolicFactorization}
  \end{figure}
  }

\end{frame}

\begin{frame}{Under the hood}

  \small{
  \begin{block}{Algorithm to eliminate the block column $k$}
    \begin{enumerate}
    \item {\color{olive}\underline{Factorize}} the diagonal block (POTRF/GETRF)
    \item {\color{blue}\underline{Solve}} off-diagonal blocks in the current column (TRSM)
    \item {\color{red}\underline{Update}} the trailing matrix with the column's
      contribution (GEMM)
    \end{enumerate}
  \end{block}
  \begin{minipage}{0.45\linewidth}
    \begin{figure}[H]
      \begin{center}
        \only<1>{\includegraphics[width=.8\textwidth]{Figs/tasks}}%
        \only<2>{\includegraphics[width=.8\textwidth]{Figs/tasks_1D_step1}}%
        \only<3>{\includegraphics[width=.8\textwidth]{Figs/tasks_1D_step2}}%
        \only<4>{\includegraphics[width=.8\textwidth]{Figs/tasks_2D_step1}}%
        \only<5>{\includegraphics[width=.8\textwidth]{Figs/tasks_2D_step2}}%
        \only<6>{\includegraphics[width=.8\textwidth]{Figs/tasks_2D_step3}}%
        \only<7>{\includegraphics[width=.8\textwidth]{Figs/tasks}}%
      \end{center}
    \end{figure}
  \end{minipage}
 \hfill
 \begin{minipage}{0.45\linewidth}
   \begin{block}{Many possibilities to do it}
     \begin{itemize}
     \item<1-> One single update $\approx$ multi-frontal
     \item<2-> 1D updates per block of columns
     \item<4-> 2D updates $\approx$ Dense factorization
     \end{itemize}
   \end{block}
 \end{minipage}
  }
\end{frame}

\begin{frame}{Software performances examples}

  \begin{center}
    \only<1>{\includegraphics[width=0.7\linewidth]{Figs/pastix_perfs_k40}\\
      PaStiX with K40 GPUs}
  \end{center}

\end{frame}

\section{MaPHyS}
\frame{\sectionpage}

\begin{frame}{Software ecosystem}

  \vspace{0.5cm}
  \textcolor{rouge_inria}{\url{https://gitlab.inria.fr/solverstack/maphys}}

  \vspace{-1.cm}
  \begin{center}
    \includegraphics[width=0.7\linewidth]{Figs/maphys_dep}
  \end{center}

  \vspace{-1.cm}
  \begin{itemize}
  \item choose between 2 direct solvers
  \item MPI + Threads
  \end{itemize}

\end{frame}

\begin{frame}{Software features}

  \begin{itemize}
  \item \textbf{Language}: Fortran 90, C, modern C++ (WIP), CMake
  \item \textbf{Precisions}: simple, double, complex, double complex
  \item \textbf{Algorithms}: CG/GMRES, Algebraic Additive Schwarz, ...
  \item \textbf{Matrices forms}: general, symmetric, SPD
  \item \textbf{Storage formats}: IJV
  \end{itemize}

\end{frame}

\begin{frame}{Under the hood}

  \hspace{2eM}
  \begin{columns}
    \begin{column}{.45\columnwidth}
      \centering
      \begin{tikzpicture}[scale=4]
        \only<2->{
          \draw[very thick,red] svg "M 7.276,8.931 C 10.723,8.627 14.474,7.986 17.842,9.210 19.732,9.95 20.542,10.876 22.009,12.216"
          svg "M 15.827,8.648 C 18.778,5.416 19.722,4.231 20.6,0.049"
          svg "M 10.3,0.049 C 9.04,2.774 8.392,3.56 8.503,6.677 8.0395,8.976 6.028,10.457 4.099,11.528 2.494,12.47 3.231,11.963 2.031,12.711";}

        \draw[thick] svg "M 6.8035713,16.585559 C 3.1154808,15.43556 -0.14690845,10.717515 0.30196912,7.3222245 0.7508467,3.9269337 1.0670019,1.6611611 4.8388684,0.47811115 8.6107348,-0.70493885 15.765373,0.72324527 19.345907,0.2591268 c 3.580535,-0.46411847 5.47078,-0.47569421 6.976329,0.9735273 1.505549,1.4492215 0.712023,4.3580171 -1.134962,6.899437 -1.846985,2.5414199 -3.355487,4.7537509 -6.446138,6.4366919 -3.09065,1.68294 -8.249474,3.166775 -11.9375647,2.016776 z";

        \only<1>{
          \path (.5, .3) node {$\Omega$};
          \path (.9, .3) node {$\partial\Omega$};
        }

        \only<2->{
          \path (.6, .37) node {$\color{red}\Gamma$};
          \path (.18, .2) node {$\Omega_1$}
          (.5, .16) node {$\Omega_2$}
          (.8, .21) node {$\Omega_3$}
          (.42, .42) node {$\Omega_4$};
        }
      \end{tikzpicture}
    \end{column}

    \begin{column}{.45\columnwidth}
      %\vspace{-9eM}
      \pause\pause
      \begin{center}
        \includegraphics[width=.9\textwidth]{Figs/LMat43.png}
      \end{center}
    \end{column}
  \end{columns}

  \start \onslide<1->
  \begin{enumerate}
  \item PDE defined on the global domain $\Omega$ \pause
  \item Partition into subdomains $\Omega_i$ \pause
  \item Discretization at the subdomain level $\Omega_i \rightarrow \Ai$
    \begin{itemize}
    \item Neumann boundary condition on $\Gamma$ \pause
    \end{itemize}
  \item \alert{DDM at the algebraic level}
  \end{enumerate}
\end{frame}

\begin{frame}{Under the hood}

  \begin{exampleblock}{\centering Domain decomposition}
    Partition unknowns: interior and interface unknowns Computed by
    SCOTCH or provided by the user
  \end{exampleblock}

  \begin{exampleblock}{\centering Direct sparse factorization}
    Direct methods to factorize interior unknowns and compute Schur
    complement on interface unknowns (using PaStiX or MUMPS)
  \end{exampleblock}

  \begin{exampleblock}{\centering Iterative solve}
    \begin{itemize}
    \item Computation of an Additive Schwarz preconditioner on the
      Schur complement, optionally with a Coarse Space Correction
    \item Iterative solve on the Schur complement (CG, GMRES
      algorithms, with BLAS/LAPACK)
    \end{itemize}
  \end{exampleblock}

\end{frame}

\begin{frame}{Software performances examples}

  \begin{center}
    \only<1>{\vspace{0.3cm}\includegraphics[width=0.9\linewidth]{Figs/BREB_tTot}\\
      Heterogeneous diffusion}
    \only<2>{\includegraphics[width=0.8\linewidth]{Figs/maphys_temps_ite_solve_log.png}\\
      Plasma propulsion (4.5 Mdof)}
  \end{center}

\end{frame}

\end{document}
